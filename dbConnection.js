var mysql = require("mysql");
var util = require("util");

var connection = mysql.createPool({
  connectionLimit: 4,
  waitForConnections: true,
  queueLimit: 0,
  host: "localhost",
  user: "root",
  password: "password",
  database:"sacs"
});
/**/
connection.query = util.promisify(connection.query);
connection.on("error", function(err) {
  console.log("########## Connection Error ##########");
  console.log(err);
});

module.exports = connection;
