const app = require("express")();
var express = require("express");
const mysql = require("mysql");
const socket = require("socket.io");
var path = require("path");
var util = require("util");
const db = require("./dbConnection");

// var pool = mysql.createPool({
//   connectionLimit: 5,
//   host: "us-cdbr-iron-east-01.cleardb.net",
//   user: "b456035381d6ee",
//   password: "48ece4e9",
//   database: "heroku_663f680511030f9"
// });

// var pool = mysql.createPool({
//   connectionLimit: 5,
//   host: "192.168.0.107:8080",
//   user: "pi",
//   password: "pi",
//   database: "mydb"
// });

// var pool = mysql.createPool({
//   host: "192.168.0.107",
//   user: "pi",
//   password: "pi",
//   database: "mydb",
//   connectionLimit: 10, // this is the max number of connections before your pool starts waiting for a release
//   multipleStatements: true // I like this because it helps prevent nested sql statements, it can be buggy though, so be careful
// });
// var connection = mysql.createPool({
//   connectionLimit: 4,
//   waitForConnections: true,
//   queueLimit: 0,
//   host: "192.168.0.107",
//   user: "pi",
//   password: "pi",
//   database: "mydb"
// });
// connection.query = util.promisify(connection.query);
// connection.on("error", function(err) {
//   console.log("########## Connection Error ##########");
//   console.log(err);
// });
server = app.listen(4000, function() {
  console.log("server is running");
  setInterval(() => sendTimeToAlarmInterface("initial"), 60000);
});

var io = socket(server);

io.on("connection", function(socket) {
  console.log("connection made");
});

app.use(express.static("public"));

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});
app.get("/test", (req, res) => {
  res.json({ testServer: "server is running" });
});

app.get("/espip", (req, res) => {
  getESPIP(res);
});

app.get("/register_ip", (req, res) => {
  let ip = req.query.ip;
  let addIPOperation = addIPToDB(ip);
  addIPOperation.then(success => {
    res.json({ status: success });
  });
});

app.get("/alarm", (req, res) => {
  let time = req.query.time;
  sendTimeToAlarmInterface(time);
  res.json({ status: "success" });
  let addAlarmData = addTimeToDB(time);
  addAlarmData.then(success => {
    res.json({ status: success });
  });
});
function sendTimeToAlarmInterface(time) {
  if ((time = "initial")) {
    let time = getAlarmTime();
    time.then(success => {
      io.sockets.emit("alarmTime", success);
    });
  } else {
    io.sockets.emit("alarmTime", time);
  }
}
//alarmtime

app.get("/alarmtime", (req, res) => {
  let time = getAlarmTime();
  time.then(success => {
    res.header("Access-Control-Allow-Origin", "*");
    res.json({ time: success });
  });
});

function addIPToDB(ip) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var promiseError = false;
      var sql = "INSERT INTO esp_ips (ip_address) VALUES ('" + ip + "')";

      pool.getConnection(function(err, connection) {
        if (err) console.log(err);
        connection.query(sql, function(err, rows) {
          if (err) {
            console.log(err);
            promiseError = true;
            connection.release();
          } else {
            console.log("ESPIP Added Successfully");
            resolve("success");
            connection.release();
          }
        });
        if (!promiseError) {
          console.log("ESPIP Added Successfully");
        } else {
          reject("Error : in promise esp not added");
        }
      });
    }, 2000);
  });
}

function addTimeToDB(time) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var promiseError = false;
      var sql = "INSERT INTO alarm_data (time) VALUES ('" + time + "')";

      pool.getConnection(function(err, connection) {
        connection.query(sql, function(err, rows) {
          if (err) {
            console.log(err);
            promiseError = true;
            connection.release();
          } else {
            console.log("alarm data Added Successfully");
            resolve("success");
            connection.release();
          }
        });
        if (!promiseError) {
          console.log("alarm data Successfully");
        } else {
          reject("Error : in promise esp not added");
        }
      });
    }, 2000);
  });
}
async function getESPIP(res) {
  try {
    let espip = await db.query(
      "SELECT ip_address FROM esp_ips ORDER BY id DESC LIMIT 1;"
    );
    if (espip.length > 0) {
      return res.status(200).json({
        message: "ok",
        statusCode: 1,
        espip: espip
      });
    } else {
      return res.status(200).json({
        message: "No ips Found",
        statusCode: 0,
        devices: ""
      });
    }
  } catch (err) {
    console.log("error : " + err);
    return res.status(200).json({
      message: "Error while getting ips",
      statusCode: -1,
      error: err
    });
  }
}

function getAlarmTime() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var promiseError = false;
      var sql = "SELECT time FROM alarm_data ORDER BY id DESC LIMIT 1;";

      pool.getConnection(function(err, connection) {
        connection.query(sql, function(err, rows) {
          if (err) {
            console.log(err);
            promiseError = true;
            connection.release();
          } else {
            let time = rows[0].time;
            console.log(" time : =  ", time);
            resolve(time);
            connection.release();
          }
        });
        if (!promiseError) {
          console.log("time fetched successfully");
        } else {
          reject("Error : in promise time from db");
        }
      });
    }, 2000);
  });
}
