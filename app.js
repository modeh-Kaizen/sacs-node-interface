const app = require('express')();
var express = require('express');
const mysql = require('mysql');
const socket = require('socket.io');
var path = require('path');
var util = require('util');
const db = require('./dbConnection');
var schedule = require('node-schedule');
var cron = require('node-cron');
var axios = require('axios');
const nodemailer = require('nodemailer');
server = app.listen(4000, function() {
	console.log('server is running');
	setInterval(() => sendTimeToAlarmInterface('initial'), 60000);
	var alarmDataCheck = cron.schedule('*/10 * * * *', async () => {
		// get time from db and update the smart service to check the weather one hour before and update if bad weather.
		let alarmTime = await getAlarmTimeForService();
		console.log(alarmTime);
		checkWeatherAndUpdateAlarm(alarmTime);
	});
	axios.get('https://sacs-server.herokuapp.com/espip').then((res) => {
		console.log(res.data['espIP']);
		let ip = res.data['espIP'];
		addFromHerokuToDBESPIp(ip);
	});
});

var io = socket(server);

io.on('connection', function(socket) {
	console.log('connection made');
});

app.use(express.static('public'));

app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname + '/index.html'));
});
app.get('/test', (req, res) => {
	res.json({ testServer: 'server is running' });
});

app.get('/espip', (req, res) => {
	getESPIP(res);
});

app.get('/register_ip', (req, res) => {
	let ip = req.query.ip;
	addIPToDB(ip, res);
});

// alarm routes
app.get('/alarm', (req, res) => {
	let time = req.query.time;
	sendTimeToAlarmInterface(time, res);
	addTimeToDB(time, res);
});
app.get('/alarmtime', (req, res) => {
	getAlarmTime(res);
});

// route
app.get('/route', (req, res) => {
	let route = req.query.route;
	console.log(route);
	io.sockets.emit('route', route);
	res.status(200).json({
		message: 'route',
		statusCode: 1,
		data: route
	});
});
async function sendTimeToAlarmInterface(time, res) {
	if ((time = 'initial')) {
		var sql = 'SELECT time FROM alarm_data ORDER BY id DESC LIMIT 1;';
		try {
			let dbTime = await db.query(sql);
			console.log(dbTime[0].time);
			if (dbTime.length > 0) {
				io.sockets.emit('alarmTime', dbTime[0].time);
			} else {
				return res.status(200).json({
					message: 'No time Found',
					statusCode: 0,
					time: ''
				});
			}
		} catch (err) {
			console.log('error : ' + err);
			return res.status(200).json({
				message: 'Error while getting ips',
				statusCode: -1,
				error: err
			});
		}
	} else {
		io.sockets.emit('alarmTime', time);
	}
}
//alarmtime
io.sockets.on('deactivateAlarm', function(data) {
	console.log(data);
	deactivateAlarm(data);
});

async function deactivateAlarm(data) {
	let sql = 'UPDATE alarm_data SET active = 0 WHERE time=' + data + '';
	try {
		let deactivate = await db.query(sql);
		return 'done';
	} catch (err) {
		console.log('error : ' + err);
		return 'error';
	}
}

async function addFromHerokuToDBESPIp(ip) {
	var sql = "INSERT INTO esp_ips (ip_address) VALUES ('" + ip + "')";
	try {
		let addIP = await db.query(sql);
		return 'Ok';
	} catch (err) {
		console.log('error : ' + err);
		return 'error';
	}
}

async function addIPToDB(ip, res) {
	var sql = "INSERT INTO esp_ips (ip_address) VALUES ('" + ip + "')";
	try {
		let addIP = await db.query(sql);

		return res.status(200).json({
			message: 'Ok',
			statusCode: 1
		});
	} catch (err) {
		console.log('error : ' + err);
		return res.status(200).json({
			message: 'Error while adding time',
			statusCode: -1,
			error: err
		});
	}
}
async function addTimeToDB(time, res) {
	try {
		let addTime = await db.query("INSERT INTO alarm_data (time) VALUES ('" + time + "')");

		return res.status(200).json({
			message: 'Ok',
			statusCode: 1
		});
	} catch (err) {
		console.log('error : ' + err);
		return res.status(200).json({
			message: 'Error while adding time',
			statusCode: -1,
			error: err
		});
	}
}
async function getESPIP(res) {
	try {
		let espip = await db.query('SELECT ip_address FROM esp_ips ORDER BY id DESC LIMIT 1;');
		if (espip.length > 0) {
			return res.status(200).json({
				message: 'ok',
				statusCode: 1,
				espip: espip[0].ip_address
			});
		} else {
			return res.status(200).json({
				message: 'No ips Found',
				statusCode: 0,
				devices: ''
			});
		}
	} catch (err) {
		console.log('error : ' + err);
		return res.status(200).json({
			message: 'Error while getting ips',
			statusCode: -1,
			error: err
		});
	}
}

async function getAlarmTime(res) {
	var sql = 'SELECT time FROM alarm_data ORDER BY id DESC LIMIT 1;';

	let time = await db.query(sql);
	try {
		let time = await db.query(sql);
		//console.log(time[0].time);
		if (time.length > 0) {
			res.header('Access-Control-Allow-Origin', '*');
			res.json({ time: time[0].time });
		} else {
			return res.status(200).json({
				message: 'No time Found',
				statusCode: 0,
				time: ''
			});
		}
	} catch (err) {
		console.log('error : ' + err);
		return res.status(200).json({
			message: 'Error while getting ips',
			statusCode: -1,
			error: err
		});
	}
}

function getWeatherData() {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			axios
				.get(
					'http://api.openweathermap.org/data/2.5/weather?appid=8bb7533926b41fe27038e5dac5d5259b&q=Palestine&units=imperial'
				)
				.then((res) => {
					let factor = 5 / 9;
					resolve((res.data['main'].temp - 32) * factor);
				});
		}, 2000);
	});
}

async function addTimeToDBForService(time) {
	try {
		let addTime = await db.query("INSERT INTO alarm_data (time,active) VALUES ('" + time + "')");

		return 'added';
	} catch (err) {
		console.log('error : ' + err);
		return 'error';
	}
}

async function checkWeatherAndUpdateAlarm(alarmTime) {
	let time = alarmTime.split(':');
	let hour = time[0];
	let min = time[1];
	h = parseInt(hour);
	m = parseInt(min);
	let hourToCheck = h - 1;
	let ServiceTime = min + ' ' + hourToCheck + ' * * *';
	//console.log(times);
	var AlarmWheatherSmartService = schedule.scheduleJob(ServiceTime, async () => {
		// get the whether data and if less than 10 then update db time - 10
		let weatherData = getWeatherData();
		weatherData.then((data) => {
			console.log('AlarmWheatherSmartService :', data);
			if (parseInt(data) < 10) {
				var date = new Date(2019, 01, 27, hour, min, 00, 00);
				var formattedTime = new Date(date - 10000 * 60);
				let times = formattedTime.toLocaleString().split(' ')[1].split(':');
				let time = times[0] + ':' + times[1];
				addTimeToDBForService(time);
			}
		});
	});
}

async function getAlarmTimeForService() {
	var sql = 'SELECT time FROM alarm_data ORDER BY id DESC LIMIT 1;';

	let time = await db.query(sql);
	try {
		let time = await db.query(sql);
		console.log(time[0].time);
		if (time.length > 0) {
			return time[0].time;
		} else {
			return '';
		}
	} catch (err) {
		console.log('error : ' + err);
		return 'error';
	}
}

// mail when face not detected

app.get('/face_not_recognized', (req, res) => {
	send(res)
		.then((resp) => {
			res.status(200).json({
				message: 'success',
				statusCode: 1
			});
		})
		.catch((err) => {
			res.status(200).json({
				message: 'error',
				statusCode: 0
			});
		});
});
async function send(res) {
	// Generate test SMTP service account from ethereal.email
	// Only needed if you don't have a real mail account for testing
	let account = await nodemailer.createTestAccount();

	// create reusable transporter object using the default SMTP transport
	let transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'maysaraodeh1@gmail.com',
			pass: 'Maysara@12341234G@Kiitos'
		}
	});

	// setup email data with unicode symbols

	let mailOptions = {
		from: '"منزلي 🏚" <foo@example.com>', // sender address
		to: 'doaa.odeh2004@gmail.com', // list of receivers
		subject: 'مرحبا ✔', // Subject line
		text: 'هناك شخص ما يحاول الدخول الى المنزل', // plain text body
		html: '<b>هناك شخص ما يحاول الدخول الى المنزل</b>',
		attachments: [
			{
				path: './public/unknown.jpg'
			}
		]
	};

	// send mail with defined transport object
	let info = await transporter.sendMail(mailOptions);

	console.log('Message sent: %s', info.messageId);
	// Preview only available when sending through an Ethereal account
	console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

	// Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
	// Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
