var socket = io.connect('http://localhost:4000');
//  $.get("http://localhost:4000/espip", function(data, status) {
//    // alert("Data: " + data + "\nStatus: " + status);
//     console.log(data);
//   });

var espip = '';
function loadESPIP() {
  axios
    .get('http://localhost:4000/espip')
    .then(function(response) {
      console.log(response.data.espip);
      espip = response.data.espip;
      console.log(espip);
      var interval = setInterval(() => getRoomTemp(), 10000);
      var interval2 = setInterval(() => getWaterTemp(), 10000);
    })
    .catch(function(error) {
      //resultElement.innerHTML = generateErrorHTMLOutput(error);
    });
}

document.addEventListener('DOMContentLoaded', () => {
  requestAnimationFrame(updateTime);
  loadESPIP();
});

var timeDataFromServer = '0';
socket.on('alarmTime', function(data) {
  console.log(data);
  var d = new Date();
  var parts = d.toString().split(' ');
  var times = parts[4].split(':');
  var finalTime = (times[0] + ':' + times[1]).toString();
  console.log(data, finalTime);
  if (finalTime === data) {
    $(document).ready(function() {
      document.getElementById('audio').play();
      socket.emit('deactivateAlarm', data);
    });
  }
});

// var interval = setInterval(() => checkAlarm(timeDataFromServer), 5000);

// // $(document).ready(function() {
// //   document.getElementById("audio").play;
// // });

// function checkAlarm(data) {
//   if (data === "0") {
//   } else {
//     var d = new Date();
//     var parts = d.toString().split(" ");
//     var times = parts[4].split(":");
//     var finalTime = (times[0] + ":" + times[1]).toString();
//     console.log(data, finalTime);
//     if (finalTime === data) {
//       $(document).ready(function() {
//         document.getElementById("audio").play();
//       });
//       setTimeout(() => {
//         console.log("delay");
//       }, 5000);
//       // $(document).ready(function() {
//       //   document.getElementById("audio").pause();
//       // });
//     }
//   }
// }
// after 5 seconds stop

var lampStatus = false;
var livingStatus = false;
function updateTime() {
  document.documentElement.style.setProperty(
    '--timer-day',
    "'" + moment().format('dd') + "'"
  );
  document.documentElement.style.setProperty(
    '--timer-hours',
    "'" + moment().format('k') + "'"
  );
  document.documentElement.style.setProperty(
    '--timer-minutes',
    "'" + moment().format('mm') + "'"
  );
  document.documentElement.style.setProperty(
    '--timer-seconds',
    "'" + moment().format('ss') + "'"
  );
  requestAnimationFrame(updateTime);
}

// control functions.
function lamp() {
  console.log('start', lampStatus);
  var status = '';
  if (lampStatus) {
    status = 'off';
  } else {
    status = 'on';
  }
  axios
    .get('http://' + espip + '/lamp=' + status)
    .then(function(res) {
      lampStatus = res.data.status;
      if (lampStatus) {
        document.getElementById('lamp-img').src = 'window-close.svg';
      } else {
        document.getElementById('lamp-img').src = 'window-open.svg';
      }
    })
    .catch(function(error) {});
}

function living() {
  console.log('start', livingStatus);
  var status = '';
  if (livingStatus) {
    status = 'off';
  } else {
    status = 'on';
  }
  axios
    .get('http://' + espip + '/living=' + status)
    .then(function(res) {
      livingStatus = res.data.status;
      if (livingStatus) {
        document.getElementById('living-img').src = 'living-on.svg';
      } else {
        document.getElementById('living-img').src = 'living-off.svg';
      }
    })
    .catch(function(error) {});
}

// water info,
function getWaterTemp() {
  axios
    .get('http://' + espip + '/waterTemp')
    .then(function(res) {
      console.log('water', res.data['water']);
      document.getElementById('waterTemp').innerHTML = res.data['water'] + 'C';
    })
    .catch(function(error) {
      //resultElement.innerHTML = generateErrorHTMLOutput(error);
    });
}

// room temp
function getRoomTemp() {
  axios
    .get('http://' + espip + '/temp')
    .then(function(res) {
      console.log('room', res.data['temp']);
      document.getElementById('roomTemp').innerHTML = res.data['temp'] + 'C';
    })
    .catch(function(error) {
      //resultElement.innerHTML = generateErrorHTMLOutput(error);
    });
}

// map
socket.on('route', function(data) {
  console.log(data);
  findRoute(data);
});
var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: new google.maps.LatLng(32.0853, 34.7818),
    zoom: 10,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
}

function findRoute(destination) {
  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();
  var cityA = new google.maps.LatLng(32.0853, 34.7818);
  directionsDisplay.setMap(map);
  //directionsDisplay.setPanel(document.getElementById('panel'));

  var request = {
    origin: cityA,
    destination: destination,
    travelMode: 'DRIVING'
  };

  directionsService.route(request, function(response, status) {
    console.log(response, status);
    if (status == 'OK') {
      directionsDisplay.setDirections(response);
    }
  });
}
